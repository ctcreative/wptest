<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress  s
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'testing_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pI*|JCIpu{sIRoM7@0R]L/?1O~6[5JIPl53wse_|B{y@M/L_r?LccR:BzdE~IeyF');
define('SECURE_AUTH_KEY',  'E,tjj|&9C9_hSlHcgjb=|YU/=[U^@o:[b-X|:)XI*Gicn}PJpVW2d]O(Dn)gDe$;');
define('LOGGED_IN_KEY',    'lk?)i=h1 T~,&4%fs!@dPaX}D-b@+u{Q;0/RFXp!N6V^+]2bza38[iK-bKW_/ tH');
define('NONCE_KEY',        'j,aG,ZpMy6Z`QD)J.k_qeaJVeXfi?/z`Hfa_}zvn,8+y{|+p+rms1dbAND@-p3[:');
define('AUTH_SALT',        '~p`oIRi A5^]xD{@C+=:o[-pAxI!<x&j*C(pm8lFH7E{-+hcE`X{uFJqfL$X**dH');
define('SECURE_AUTH_SALT', 'h}G3X|#x*:7v{|0IqskD/e[ d%8XcP$tt@#Lo7cX]8N7HZ]kmUF2v$NRRLb{$P0]');
define('LOGGED_IN_SALT',   'DgY-~};q&6y)T;<lD: q4H-ww)4&PGn.my@5q7ST^)em,^ybT2rMPCJ=#GG)[2kw');
define('NONCE_SALT',       '_T/g b+z%9Fk7-Z#b5-CI(f2UIISgmjRf&TpR18T0ad,Pf;Y,`y.z_|MV2R<EZr~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
